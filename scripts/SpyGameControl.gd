extends Node

class_name SpyGameControl

onready var game = get_parent()
onready var chat_control: ChatControl = game.get_node("../ChatControl")
onready var lobby_control: LobbyControl = game.get_node("../LobbyControl")
onready var lobby_view: LobbyView = lobby_control.get_node("LobbyView")

onready var category_label: Label = $CenterVBox/CategoryLabel
onready var category_words: GridContainer = $CenterVBox/CategoryWords
onready var role_label: Label = $CenterVBox/RoleLabel
onready var phase_label: Label = $CornerVBox/PhaseLabel

onready var infos: VBoxContainer = $CornerVBox/InfosVBox
const HINT = 0
const WAIT_FOR_HINT = 1
const VOTE = 2
const WAIT_FOR_VOTE = 3
const GUESS = 4
const WAIT_FOR_GUESS = 5
const WAIT_FOR_LEADER = 6
const CONTINUE = 7
const SKIP = 8

onready var continue_button: Button = $CornerVBox/InfosVBox/ContinueButton
onready var skip_button: Button = $CornerVBox/InfosVBox/SkipButton

var panel_font = load("res://assets/Font_Bold.tres")
var panel_font_s = load("res://assets/Font_Bold_S.tres")
var panel_font_xs = load("res://assets/Font_Bold_XS.tres")
var last_turn_client_index: int

func on_init():
	set_visible(false)

func on_start():
	for client in game.model.clients:
		lobby_view.set_flag(client.id, false)
	last_turn_client_index = -1
	set_visible(true)

func set_visible(visible):
	$CenterVBox.visible = visible
	$CornerVBox.visible = visible

func set_label_visible(index: int):
	for child in infos.get_children():
		child.visible = false
	infos.get_child(index).visible = true
	
	var id = game.get_own_client().id
	var client = lobby_control.get_client(id)
	if client.isLeader and index <= WAIT_FOR_HINT:
		infos.get_child(SKIP).visible = true

func set_is_spy(is_spy: bool):
	var text = "You're the wannabe!" if is_spy else "You're cool!"
	role_label.text = text
	role_label.add_color_override(
		"font_color",
		Color.lime if is_spy else Color.gold
	)

func set_client_hint(_id, _hint, _is_self):
	pass

func set_client_vote_id(id, vote_id, is_override, is_self):
	if is_self:
		set_label_visible(WAIT_FOR_VOTE)
	var name = chat_control.get_client_name_text(id)
	var vote_name = chat_control.get_client_name_text(vote_id)
	if is_override:
		chat_control.log_text(("%s hat sich für %s umentschieden.")
			% [name, vote_name])
	else:
		chat_control.log_text(("%s hat für %s abgestimmt.")
			% [name, vote_name])
	lobby_view.set_flag(id, false)

func set_reveal(spy_id: String, vote_id: String):
	var spy_name = chat_control.get_client_name_text(spy_id)
	var vote_name = chat_control.get_client_name_text(vote_id)
	if spy_id == vote_id:
		chat_control.log_text(("Wannabe %s ist aufgeflogen, " +
			"kann aber das Geheimwort noch erraten.") % [spy_name])
	else:
		chat_control.log_text(("Ihr habt für %s abgestimmt " +
			"und Wannabe %s kam davon.") % [vote_name, spy_name])

func set_phase_and_turn(phase, turn_client_index, is_own_turn):
	if last_turn_client_index != -1:
		lobby_view.set_flag(game.model.clients[last_turn_client_index].id, false)
	match phase:
		game.HINT:
			phase_label.text = "Hinweise sammeln"
			set_label_visible(HINT if is_own_turn else WAIT_FOR_HINT)
			lobby_view.set_flag(game.model.clients[turn_client_index].id, true)
			last_turn_client_index = turn_client_index
		game.VOTE:
			phase_label.text = "Abstimmen"
			set_label_visible(VOTE)
			last_turn_client_index = -1
			for client in game.model.clients:
				lobby_view.set_flag(client.id, true)
		game.GUESS:
			phase_label.text = "Wort erraten"
			set_label_visible(GUESS if game.is_spy() else WAIT_FOR_GUESS)
			lobby_view.set_flag(game.model.spyId, true)
		game.STANDBY:
			phase_label.text = "Runde vorbei"
			set_label_visible(CONTINUE if game.is_leader() else WAIT_FOR_LEADER)
			lobby_view.set_flag(game.get_leader().id, true)

func set_category_name(name):
	category_label.text = name

func set_category_words(words: Array):
	for i in range(0, words.size()):
		var word_panel = category_words.get_child(i)
		word_panel.data = i # store index in button
		if words[i].length() < 8:
			word_panel.set("custom_fonts/font", panel_font)
		elif words[i].length() < 10:
			word_panel.set("custom_fonts/font", panel_font_s)
		else:
			word_panel.set("custom_fonts/font", panel_font_xs)
		word_panel.text = words[i]
		# remove color override
		word_panel.set("custom_colors/font_color", null)
		word_panel.set("custom_colors/font_color_hover", null)

func set_secret_index(secret_index):
	if secret_index == -1:
		return
	var word_panel: Button = category_words.get_child(secret_index)
	word_panel.add_color_override("font_color", Color.gold)
	word_panel.add_color_override("font_color_hover", Color.gold)

func set_guess_index(guess_index):
	if guess_index == -1:
		return
	var name = chat_control.get_client_name_text(game.model.spyId)
	var category = game.model.category
	var secret = category.words[category.secretIndex]
	var guess = category.words[guess_index]
	var guessed_correctly = (category.secretIndex == guess_index)
	chat_control.log_text("Wannabe %s hat auf %s getippt." % [name, guess])
	if guessed_correctly:
		chat_control.log_text(("[color=#FFD600]%s[/color] " +
			"Das Geheimwort war [color=#FFD600]%s[/color].")
			% [get_random_correct_text(), secret])
	else:
		chat_control.log_text(("[color=#FFD600]%s[/color] " +
			"Das Geheimwort war nicht [color=#FFD600]%s[/color], " +
			"sondern [color=#FFD600]%s[/color].")
			% [get_random_incorrect_text(), guess, secret])
	lobby_view.set_flag(game.model.spyId, false)

func get_random_correct_text():
	var texts = [
		"Das war richtig!",
		"Richtig!",
		"Korrekt!",
		"Yess! (•̀•́)و",
		"Volltreffer!",
		"Voll ins Schwarze!",
		"Gut geraten!",
		"Ihr wurdet wohl verraten!",
		"Nice!",
	]
	return texts[randi() % texts.size()]

func get_random_incorrect_text():
	var texts = [
		"Das war falsch!",
		"Falsch!",
		"Nope!",
		"Knapp daneben!",
		"Zu dumm...",
		"Pech gehabt!",
		# "(ಠ_ಠ)┌∩┐",
		"(╯°□°）╯︵ ┻━┻",
		"(>ლ)",
		"༼ ༎ຶ ෴ ༎ຶ༽",
		# "Leider nein. ¯\\_(ツ)_/¯",
	]
	return texts[randi() % texts.size()]

func set_client_scores(clientScores: Array):
	var log_data = []
	for i in range(clientScores.size()):
		var id = game.model.clients[i].id
		var client = lobby_control.get_client(id)
		if client == null:
			continue
		log_data.append({
			"name": client.displayName,
			"score": clientScores[i],
		})
	for data in log_data:
		chat_control.log_text("[color=#FFD600]%d[/color] Punkt%s: [color=lime]%s[/color]"
			% [data.score, "e" if data.score != 1 else "", data.name])
