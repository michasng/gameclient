extends Node

class_name SocketClient

# The URL we will connect to
# export var websocket_url = "ws://echo.websocket.org"
export var use_prod_url = true
export var prod_websocket_url = "wss://server.netsky.io"
export var dev_websocket_url = "ws://localhost:5000"

# Our WebSocketClient instance
var _client = WebSocketClient.new()

signal event_received
signal error_received

onready var keep_alive_timer = get_node("../KeepAliveTimer")

signal connected
signal disconnected

func _ready():
	# Connect base signals to get notified of connection open, close, and errors.
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	# This signal is emitted when not using the Multiplayer API every time
	# a full packet is received.
	# Alternatively, you could check get_peer(1).get_available_packets() in a loop.
	_client.connect("data_received", self, "_on_data")
	keep_alive_timer.connect("timeout", self, "send_keep_alive")

func connect_to_server():
	# Initiate connection to the given URL.
	var url = prod_websocket_url if use_prod_url else dev_websocket_url
	var err = _client.connect_to_url(url)
	if err != OK:
		print("Unable to connect")
		set_process(false)

func reconnect_to_server():
	_client.disconnect_from_host()
	call_deferred("connect_to_server")

func _closed(was_clean = false):
	# was_clean will tell you if the disconnection was correctly notified
	# by the remote peer before closing the socket.
	prints("Closed, clean:", was_clean)
	set_process(false)
	keep_alive_timer.stop()
	emit_signal("disconnected")

func _connected(proto = ""):
	# This is called on connection, "proto" will be the selected WebSocket
	# sub-protocol (which is optional)
	prints("Connected to WebSocket", proto)
	# You MUST always use get_peer(1).put_packet to send data to server,
	# and not put_packet directly when not using the MultiplayerAPI.
	# send("echo", {"test": "ok"})
	keep_alive_timer.init()
	emit_signal("connected")

func send(event: String, data = {}):
	var req = {
		"event": event,
		"data": data
	}
	# converts to a JSON string, then to a byte array
	var packet = JSON.print(req).to_utf8()
	prints('sending event:', event, data)
	_client.get_peer(1).put_packet(packet)

func _on_data():
	# Print the received packet, you MUST always use get_peer(1).get_packet
	# to receive data from server, and not get_packet directly when not
	# using the MultiplayerAPI.
	var str_packet = _client.get_peer(1).get_packet().get_string_from_utf8()
	var packet = JSON.parse(str_packet).result as Dictionary
	prints("Got data from server:", packet)
	if packet.has("event"):
		emit_signal("event_received", packet.get("event"), packet.get("data"))
	else:
		emit_signal("error_received", packet.get("error"))

func _process(_delta):
	# Call this in _process or _physics_process. Data transfer, and signals
	# emission will only happen when calling this function.
	_client.poll()

func send_keep_alive():
	send("echo", "ping")
