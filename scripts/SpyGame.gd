extends Node

class_name SpyGame

var game_control: SpyGameControl
onready var lobby_control: LobbyControl = get_node("../LobbyControl")

onready var game_manager: GameManager = get_node("../GameManager")
onready var game_service: SpyGameService = get_node("../SpyGameService")
onready var lobby_service = get_node("../LobbyService")
onready var chat_service = get_node("../ChatService")

const HINT = 'hint'
const VOTE = 'vote'
const GUESS = 'guess'
const STANDBY = 'standby'

var model = {
	"clients": [], # id, orderIndex, score, hint, voteId
	"spyId": null, # type String or Nil (null value)
	"voteId": null,
	"phase": null,
	"turnClientIndex": -1,
	"category": null, # name, words, secretIndex, guessIndex
}

var client_index

func _ready():
	# warning-ignore:return_value_discarded
	game_service.connect("init_game", self, "on_init_game")
	# warning-ignore:return_value_discarded
	game_service.connect("start", self, "on_start")
	# warning-ignore:return_value_discarded
	game_service.connect("hint", self, "on_hint")
	# warning-ignore:return_value_discarded
	game_service.connect("vote", self, "on_vote")
	# warning-ignore:return_value_discarded
	game_service.connect("reveal", self, "on_reveal")
	# warning-ignore:return_value_discarded
	game_service.connect("guess", self, "on_guess")
	# warning-ignore:return_value_discarded
	game_service.connect("score", self, "on_score")
	# warning-ignore:return_value_discarded
	game_service.connect("quit_game", self, "on_quit_game")
	# warning-ignore:return_value_discarded
	lobby_control.lobby_view.connect("label_pressed", self, "on_client_selected")
	# warning-ignore:return_value_discarded
	game_manager.connect("game_left", self, "on_game_left")

func set_clients(clients):
	clients.sort_custom(self, "compare_clients")
	model.clients = clients
	update_client_index()

func compare_clients(c1, c2):
	return c1.orderIndex < c2.orderIndex

func set_spy_id(spy_id):
	model.spyId = spy_id
	game_control.set_is_spy(spy_id == get_own_client().id)

func set_client_hint(id, hint):
	var client = get_client(id)
	client.hint = hint
	var is_self = (id == get_own_client().id)
	game_control.set_client_hint(id, hint, is_self)

func set_client_vote_id(id, vote_id):
	var client = get_client(id)
	var is_override = (client.voteId != null)
	client.voteId = vote_id
	var is_self = (id == get_own_client().id)
	game_control.set_client_vote_id(id, vote_id, is_override, is_self)

func set_vote_id(vote_id):
	model.voteId = vote_id
	game_control.set_reveal(model.spyId, vote_id)

func set_phase_and_turn(phase, turn_client_index = -1):
	prints("set phase and turn")
	model.phase = phase
	model.turnClientIndex = turn_client_index
	var is_own_turn = (turn_client_index == client_index)
	game_control.set_phase_and_turn(phase, turn_client_index, is_own_turn)

func set_category(category):
	model.category = category
	game_control.set_category_name(category.name)
	game_control.set_category_words(category.words)
	set_guess_index(-1) # reset guess

func set_secret_index(secret_index):
	model.category.secretIndex = secret_index
	game_control.set_secret_index(secret_index)

func set_guess_index(guess_index):
	model.category.guessIndex = guess_index
	game_control.set_guess_index(guess_index)

func set_client_scores(client_scores):
	for i in range(client_scores.size()):
		model.clients[i].score = client_scores[i]
	game_control.set_client_scores(client_scores)

func on_init_game(_data):
	var control_resource = load("res://scenes/SpyGameControl.tscn")
	game_control = control_resource.instance()
	add_child(game_control)
	for word_panel in game_control.category_words.get_children():
		# warning-ignore:return_value_discarded
		word_panel.connect("button_pressed", self, "on_word_pressed")
	# warning-ignore:return_value_discarded
	game_control.continue_button.connect("pressed", game_service, "send_start")
	# warning-ignore:return_value_discarded
	game_control.skip_button.connect("pressed", game_service, "send_start")
	game_control.on_init()

func on_start(data):
	# order is important, reset control variables first
	game_control.on_start()
	set_clients(data.clients)
	set_spy_id(data.spyId)
	set_category(data.category)
	set_secret_index(data.secretIndex)
	set_phase_and_turn(HINT, 0)

func on_hint(data):
	prints("hint", data.originClientId, data.hint)
	set_client_hint(data.originClientId, data.hint)
	
	# next turn
	var next_turn_client_index = model.turnClientIndex + 1
	if next_turn_client_index == model.clients.size():
		next_turn_client_index = -1
		set_phase_and_turn(VOTE, -1)
	else:
		set_phase_and_turn(HINT, next_turn_client_index) # no phase-change yet

func on_vote(data):
	prints("vote", data.originClientId, data.voteId)
	set_client_vote_id(data.originClientId, data.voteId)

func on_reveal(data):
	prints("reveal", data.voteId, data.spyId)
	set_spy_id(data.spyId)
	set_vote_id(data.voteId)
	if data.voteId == data.spyId: # spy has been caught
		# data.secretIndex is -1
		set_phase_and_turn(GUESS)
	else: # spy was not caught
		set_secret_index(data.secretIndex)
		set_phase_and_turn(STANDBY)

func on_word_pressed(index):
	prints("word pressed", index)
	if is_phase(GUESS) and is_spy():
		game_service.send_guess(index)

func on_guess(data):
	prints("guess", data.secretIndex, data.guessIndex)
	set_secret_index(data.secretIndex)
	set_guess_index(data.guessIndex)
	set_phase_and_turn(STANDBY)

func on_score(data):
	prints("score", data.clientScores)
	set_client_scores(data.clientScores)

func on_game_left(is_origin, client):
	prints("game left", is_origin, client)
	if is_origin or client.isLeader:
		leave_game()
		return
	var index = get_client_index(client.id)
	model.clients.remove(index)
	set_clients(model.clients)
	set_phase_and_turn(STANDBY)

func on_quit_game(_data):
	print("quit game")
	leave_game()

func on_client_selected(id):
	prints("client selected", id)
	if is_phase(VOTE):
		game_service.send_vote(id)

func send_chat_message(text: String):
	if is_phase(HINT) and is_own_turn():
		game_service.send_hint(text)
	else:
		chat_service.send_chat_message(text)

func leave_game():
	if game_control != null:
		game_control.queue_free()
		game_control = null

func update_client_index():
	var index = 0
	for client in model.clients:
		if client.id == game_manager.own_client.id:
			client_index = index
			break
		index += 1

func get_client_index(id):
	var index = 0
	for client in model.clients:
		if client.id == id:
			return index
		index += 1
	return -1

func get_client(id):
	for client in model.clients:
		if client.id == id:
			return client
	return null

func get_own_client():
	return model.clients[client_index]

func is_own_turn() -> bool:
	return model.turnClientIndex == get_own_client().orderIndex

func is_spy() -> bool:
	return model.spyId == get_own_client().id

func get_leader():
	var client = lobby_control.get_leader()
	return get_client(client.id)

func is_leader() -> bool:
	var game_client = get_own_client()
	var client = lobby_control.get_client(game_client.id)
	return client.isLeader

func is_phase(phase) -> bool:
	return model.phase == phase
