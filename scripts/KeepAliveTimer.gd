extends Timer

const WAIT_TIME = 60

func init():
	self.start(WAIT_TIME)
	# warning-ignore:return_value_discarded
	self.connect("timeout", self, "on_timeout")

func on_timeout():
	self.start(WAIT_TIME)
