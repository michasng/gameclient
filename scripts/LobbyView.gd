extends Node

class_name LobbyView

onready var lobby_node = get_parent()

onready var lobby_service: LobbyService = get_node("../../LobbyService")
onready var socket_client: SocketClient = get_node("../../SocketClient")

onready var lobby_sub_view: Control = $LobbySubView
onready var waitingLabel: Label = $LobbySubView/VBox/WaitingLabel
onready var name_edit: LineEdit = $LobbySubView/VBox/NameEdit
onready var lobby_list: ItemList = $LobbySubView/VBox/LobbyList
onready var leave_button: Button = $LobbySubView/VBox/HBox/LeaveButton
onready var create_button: Button = $LobbySubView/VBox/HBox/HBox/CreateButton
onready var join_button: Button = $LobbySubView/VBox/HBox/HBox/JoinButton
onready var ready_button: Button = $LobbySubView/VBox/HBox/HBox/ReadyButton
onready var connect_button: Button = $LobbySubView/StatusHBox/ConnectButton

onready var game_sub_view: Control = $GameSubView
onready var game_clients_box: Control = $GameSubView/ClientsVBox
onready var leave_lobby_button: Button = $GameSubView/HBox/LeaveButton
onready var quit_game_button: Button = $GameSubView/HBox/QuitButton

var flag_icon = load("res://assets/flag32.png")
var connected_icon = load("res://assets/green_dot.png")
var disconnected_icon = load("res://assets/red_dot.png")

signal label_pressed


func _ready():
	# warning-ignore:return_value_discarded
	socket_client.connect("connected", self, "on_connected")
	# warning-ignore:return_value_discarded
	socket_client.connect("disconnected", self, "on_disconnected")
	# warning-ignore:return_value_discarded
	lobby_list.connect("item_selected", self, "lobby_selected")
	# warning-ignore:return_value_discarded
	leave_button.connect("pressed", self, "leave")
	# warning-ignore:return_value_discarded
	name_edit.connect("text_entered", self, "name_entered")
	# warning-ignore:return_value_discarded
	name_edit.connect("text_changed", self, "name_changed")
	# warning-ignore:return_value_discarded
	create_button.connect("pressed", self, "create")
	# warning-ignore:return_value_discarded
	join_button.connect("pressed", self, "join")
	# warning-ignore:return_value_discarded
	ready_button.connect("pressed", self, "ready")
	# warning-ignore:return_value_discarded	
	connect_button.connect("pressed", self, "connect_to_server")
	# warning-ignore:return_value_discarded
	leave_lobby_button.connect("pressed", self, "leave")
	# warning-ignore:return_value_discarded
	quit_game_button.connect("pressed", self, "quit")

	update_buttons_disabled()

# sending client events

func lobby_selected(_index: int):
	update_buttons_disabled()

func leave():
	lobby_service.send_lobby_leave()

func name_entered(_name: String):
	if lobby_list.is_anything_selected():
		join()
	else:
		create()

func name_changed(_name: String):
	update_buttons_disabled()

func update_buttons_disabled():
	var displayName = name_edit.text
	create_button.disabled = displayName.empty()
	join_button.disabled = displayName.empty() or \
		not lobby_list.is_anything_selected()

func create():
	var displayName = name_edit.text
	if displayName.empty():
		prints("no name entered")
		return
	lobby_service.send_lobby_create(displayName)

func join():
	if not lobby_list.is_anything_selected():
		prints("no lobby selected")
		return
	var selected_index = lobby_list.get_selected_items()[0]
	prints("selected", selected_index)
	var lobby = lobby_node.get_lobby(selected_index)
	var displayName = name_edit.text
	if displayName.empty():
		prints("no name entered")
		return
	lobby_service.send_lobby_join(lobby.id, displayName)

func connect_to_server():
	if connect_button.icon == disconnected_icon:
		print("try to reconnect")
		socket_client.reconnect_to_server()

func ready():
	lobby_node.init_game()

func quit():
	lobby_node.quit_game()

func on_connected():
	connect_button.icon = connected_icon

func on_disconnected():
	connect_button.icon = disconnected_icon

# building ui

func add_client(client, lobby_idx):
	if lobby_node.is_in_overview():
		var lobby_text = _get_lobby_text(lobby_node.get_lobby(lobby_idx))
		if lobby_idx != -1: # existing lobby
			lobby_list.set_item_text(lobby_idx, lobby_text)
		else: # new lobby
			lobby_list.add_item(lobby_text)
	elif lobby_node.is_in_lobby():
		lobby_list.add_item(_get_client_text(client))
	elif lobby_node.is_in_game():
		add_game_client(client)

func add_game_client(client):
	var resource = load("res://scenes/PlayerLabel.tscn")
	var player_label: PlayerLabel = resource.instance()
	player_label.set_text(_get_client_text(client))
	player_label.data = client.id
	# warning-ignore:return_value_discarded
	player_label.connect("button_pressed", self, "on_label_pressed")
	game_clients_box.add_child(player_label)

func on_label_pressed(data):
	emit_signal("label_pressed", data)

func remove_client(client_idx, lobby_idx, lobby_removed):
	if lobby_node.is_in_overview():
		if lobby_removed:
			lobby_list.remove_item(lobby_idx)
		else:
			var lobby_text = _get_lobby_text(lobby_node.get_lobby(lobby_idx))
			lobby_list.set_item_text(lobby_idx, lobby_text)
	elif lobby_node.is_in_lobby():
		lobby_list.remove_item(client_idx)
	elif lobby_node.is_in_game():
		remove_game_client(client_idx)

func remove_game_client(client_idx):
	# child at index: 0 = buttons
	var player_label = game_clients_box.get_child(client_idx)
	player_label.queue_free()

# building UI

func _get_lobby_text(lobby) -> String:
	return "%d Player in Lobby %s"%[lobby.clients.size(), lobby.id]

func _get_client_text(client) -> String:
	return client.displayName

func rebuild_ui(is_lobby_leader):
	var is_in_game = lobby_node.is_in_game()
	var is_in_lobby = lobby_node.is_in_lobby()
	var is_in_overview = lobby_node.is_in_overview()
	
	lobby_sub_view.visible = !is_in_game
	game_sub_view.visible = is_in_game
	quit_game_button.visible = is_lobby_leader
	name_edit.visible = is_in_overview
	create_button.visible = is_in_overview
	join_button.visible = is_in_overview
	waitingLabel.visible = is_in_lobby
	leave_button.visible = is_in_lobby
	ready_button.visible = is_in_lobby and is_lobby_leader
	
	lobby_list.clear()
	if is_in_overview:
		for lobby in lobby_node.lobbies:
			lobby_list.add_item(_get_lobby_text(lobby))
	elif is_in_lobby:
		for client in lobby_node.current_lobby.clients:
			lobby_list.add_item(_get_client_text(client))
	elif is_in_game:
		for i in range(game_clients_box.get_child_count()):
			remove_game_client(i)
		for client in lobby_node.current_lobby.clients:
			add_game_client(client)

func set_flag(id, flagged):
	prints("set flag", str(flagged))
	for player_label in game_clients_box.get_children():
		if player_label.data == id:
			player_label.set_icon(flag_icon if flagged else null)
