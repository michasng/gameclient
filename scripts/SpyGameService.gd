extends Node

class_name SpyGameService

onready var socket = get_node("../SocketClient")

signal init_game
signal start
signal hint
signal vote
signal reveal
signal guess
signal score
signal quit_game


func _ready():
	socket.connect("event_received", self, "_on_event_received")

func _on_event_received(event, data):
	match event:
		"spy-init-game":
			emit_signal("init_game", data)
		"spy-start":
			emit_signal("start", data)
		"spy-hint":
			emit_signal("hint", data)
		"spy-vote":
			emit_signal("vote", data)
		"spy-reveal":
			emit_signal("reveal", data)
		"spy-guess":
			emit_signal("guess", data)
		"spy-score":
			emit_signal("score", data)
		"spy-quit-game":
			emit_signal("quit_game", data)

func send_init_game():
	socket.send("spy-init-game")

func send_start():
	socket.send("spy-start")

func send_hint(hint: String):
	socket.send("spy-hint", {
		"hint": hint,
	})

func send_vote(voteId: String):
	socket.send("spy-vote", {
		"voteId": voteId,
	})

func send_guess(guess_index: int):
	socket.send("spy-guess", {
		"guessIndex": guess_index,
	})

func send_quit_game():
	socket.send("spy-quit-game")
