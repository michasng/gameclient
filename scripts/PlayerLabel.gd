extends Control

class_name PlayerLabel

signal button_pressed

var data

func _ready():
	# warning-ignore:return_value_discarded
	$Button.connect("pressed", self, "_on_pressed")

func _on_pressed():
	emit_signal("button_pressed", data)

func set_text(text):
	# determine children in func, bc _ready might not have run yet
	$Button.text = text

func set_icon(icon):
	$Texture.texture = icon
