extends Node

class_name GameManager

onready var chat_service: ChatService = get_node("../ChatService")
onready var spy_game_service: SpyGameService = get_node("../SpyGameService")
onready var spy_game = get_node("../SpyGame")

# abstract game services
var game_service = null
var game = null
var own_client = null

signal init_game
signal quit_game
# emitted, when a client leaves the current lobby
signal game_left(is_origin, client)


func _ready():
	# warning-ignore:return_value_discarded
	spy_game_service.connect("init_game", self, "on_init_spy_game")
	# warning-ignore:return_value_discarded
	spy_game_service.connect("quit_game", self, "on_quit_game")

func is_lobby_leader() -> bool:
	if own_client == null:
		return false
	return own_client.isLeader

func on_init_spy_game(data):
	game_service = spy_game_service
	game = spy_game
	emit_signal("init_game", data)

func on_quit_game(data):
	game_service = null
	game = null
	emit_signal("quit_game", data)

func on_game_left(is_origin, client):
	emit_signal("game_left", is_origin, client)

func send_init_game():
	# ToDo: game selection before sending the proper message
	spy_game_service.send_init_game()

func send_quit_game():
	game_service.send_quit_game()

func send_chat_message(text: String):
	if game == null:
		chat_service.send_chat_message(text)
	else:
		game.send_chat_message(text)
