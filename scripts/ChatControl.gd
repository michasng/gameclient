extends CanvasItem

class_name ChatControl

onready var chat_service = get_node("../ChatService")
onready var lobby_service = get_node("../LobbyService")
onready var lobby_control = get_node("../LobbyControl")
onready var game_mananger = get_node("../GameManager")

onready var chat_log: RichTextLabel = $VBox/ChatLog
onready var chat_edit: LineEdit = $VBox/HBox/ChatEdit
onready var send_button: Button = $VBox/HBox/SendButton


func _ready():
	# warning-ignore:return_value_discarded
	send_button.connect("pressed", self, "send")
	# warning-ignore:return_value_discarded
	chat_edit.connect("text_entered", self, "send_text")
	
	# warning-ignore:return_value_discarded
	chat_service.connect("chat_list", self, "on_chat_list")
	# warning-ignore:return_value_discarded
	chat_service.connect("chat_message", self, "on_chat_message")
	# warning-ignore:return_value_discarded
	lobby_service.connect("lobby_join", self, "on_lobby_join")
	# warning-ignore:return_value_discarded
	lobby_service.connect("lobby_leave", self, "on_lobby_leave")

func send():
	send_text(chat_edit.text)

func send_text(text: String):
	if text.empty():
		return
	game_mananger.send_chat_message(text)
	chat_edit.clear()

func chat_list():
	chat_service.send_chat_list()

func on_chat_message(data):
	_log_message(data.message)

func on_chat_list(data):
	for message in data.messages:
		_log_message(message)

func _log_message(message):
	var datetime = Globals.get_datetime_from_unix_time(message.timestamp)
	var time_str = Globals.get_time_string(datetime)
	var name = get_client_name_text(message.clientId)
	time_str = ("%s %s: "%[time_str, name])
	if message.highlighted:
		log_text(time_str + ("[b]%s[/b]"%[message.text]))
	else:
		log_text(time_str + message.text)

func get_client_name_text(id):
	if id == null:
		return "[color=#000000]unknown[/color]"
	else:
		var client = lobby_control.get_client(id)
		return "[color=lime]%s[/color]"%[client.displayName]

func log_text(text: String):
	if not chat_log.text.empty():
		chat_log.newline()
	chat_log.append_bbcode(text)

func on_lobby_join(data):
	if data.isOrigin:
		self.visible = true
		chat_list()

func on_lobby_leave(data):
	if data.isOrigin:
		chat_log.clear()
		self.visible = false
