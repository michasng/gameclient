extends Node

class_name ChatService

onready var socket = get_node("../SocketClient")

signal chat_list
signal chat_message


func _ready():
	socket.connect("event_received", self, "_on_event_received")

func _on_event_received(event, data):
	match event:
		"chat-list":
			emit_signal("chat_list", data)
		"chat-message":
			emit_signal("chat_message", data)

func send_chat_message(text: String):
	socket.send("chat-message", {
		"message": {
			"text": text,
		},
	})

func send_chat_list():
	socket.send("chat-list")
