extends Node

# utility

func find_by_id(array, id) -> int:
	var index = 0
	for e in array:
		if e.id == id:
			return index
		index += 1
	return -1

func get_datetime_from_unix_time(unix_time) -> Dictionary:
	var time_zone = OS.get_time_zone_info()
	var bias_seconds = time_zone.bias * 60 # time_zone.bias is in minutes
	var local_timestamp = unix_time + bias_seconds
	return OS.get_datetime_from_unix_time(local_timestamp)

func get_time_string(datetime) -> String:
	return "%d:%d:%d"%[datetime.hour, datetime.minute, datetime.second]

