extends Node

class_name LobbyControl

onready var socket_client = get_node("../SocketClient")
onready var lobby_service = get_node("../LobbyService")
onready var game_manager = get_node("../GameManager")

onready var lobby_view: LobbyView = $LobbyView

const LOBBY_OVERVIEW = 0
const LOBBY = 1
const GAME = 2

var state
var lobbies = []
var current_lobby


func _ready():
	# warning-ignore:return_value_discarded
	lobby_service.connect("lobby_list", self, "on_lobby_list")
	# warning-ignore:return_value_discarded
	lobby_service.connect("lobby_join", self, "on_lobby_join")
	# warning-ignore:return_value_discarded
	lobby_service.connect("lobby_leave", self, "on_lobby_leave")
	# warning-ignore:return_value_discarded
	game_manager.connect("init_game", self, "on_init_game")
	# warning-ignore:return_value_discarded
	game_manager.connect("quit_game", self, "on_quit_game")

	call_deferred("set_state", LOBBY_OVERVIEW)
	socket_client.call_deferred("connect_to_server")

# reacting to server events

func on_lobby_list(data):
	print('on lobby list')
	lobbies = data.lobbies
	lobby_view.rebuild_ui(game_manager.is_lobby_leader())

func on_lobby_join(data):
	var lobby_idx = Globals.find_by_id(lobbies, data.lobbyId)
	if lobby_idx != -1: # update an existing lobby
		lobbies[lobby_idx].clients.append(data.client)
	else: # add a new lobby
		lobbies.append({
			"id": data.lobbyId,
			"clients": [data.client],
		})
	
	if data.isOrigin: # join lobby yourself
		current_lobby = lobbies[lobby_idx]
		game_manager.own_client = data.client
		set_state(LOBBY)
	elif state == LOBBY_OVERVIEW:
		lobby_view.add_client(data.client, lobby_idx)
	elif data.lobbyId == current_lobby.id: # add client to current lobby
		lobby_view.add_client(data.client, lobby_idx)

func on_lobby_leave(data):
	var lobby_idx = Globals.find_by_id(lobbies, data.lobbyId)
	if lobby_idx == -1:
		printerr("Could not find lobby to leave", data)
		return
	var lobby = lobbies[lobby_idx]
	var client_idx = Globals.find_by_id(lobby.clients, data.clientId)
	if client_idx == -1:
		printerr("Could not find leaving client", data)
		return
	var client = lobby.clients[client_idx]
	
	lobby.clients.remove(client_idx)
	var lobby_removed = lobby.clients.empty()
	if lobby_removed:
		lobbies.remove(lobby_idx)
	
	# if a client left the current lobby
	if lobby == current_lobby:
		game_manager.on_game_left(data.isOrigin, client)
	
	# update ui
	if state == LOBBY_OVERVIEW:
		lobby_view.remove_client(client_idx, lobby_idx, lobby_removed)
	elif lobby.id == current_lobby.id:
		if lobby_removed or data.isOrigin:
			current_lobby = null
			game_manager.own_client = null
			set_state(LOBBY_OVERVIEW)
		else:
			lobby_view.remove_client(client_idx, lobby_idx, lobby_removed)

func on_init_game(_data):
	set_state(GAME)

func on_quit_game(_data):
	set_state(LOBBY)

func set_state(new_state):
	state = new_state
	lobby_view.rebuild_ui(game_manager.is_lobby_leader())

func is_in_overview() -> bool:
	return state == LOBBY_OVERVIEW

func is_in_lobby() -> bool:
	return state == LOBBY

func is_in_game() -> bool:
	return state == GAME

func init_game():
	game_manager.send_init_game()

func quit_game():
	game_manager.send_quit_game()

func get_lobby(index: int):
	return lobbies[index]

func get_leader():
	for client in current_lobby.clients:
		if client.isLeader:
			return client
	return null

func get_client(id: String):
	for client in current_lobby.clients:
		if client.id == id:
			return client
