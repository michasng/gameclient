extends Button

class_name DataButton

signal button_pressed

var data

func _ready():
	# warning-ignore:return_value_discarded
	connect("pressed", self, "_on_pressed")

func _on_pressed():
	emit_signal("button_pressed", data)
