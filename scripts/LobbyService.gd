extends Node

class_name LobbyService

onready var socket = get_node("../SocketClient")

signal lobby_list
signal lobby_join
signal lobby_leave


func _ready():
	socket.connect("event_received", self, "_on_event_received")
	socket.connect("error_received", self, "_on_error_received")

func _on_event_received(event, data):
	match event:
		"lobby-list":
			emit_signal("lobby_list", data)
		"lobby-join":
			emit_signal("lobby_join", data)
		"lobby-leave":
			emit_signal("lobby_leave", data)

func _on_error_received(error):
	printerr("error received: ", error)

func send_lobby_create(display_name: String):
	socket.send("lobby-create", {
		"client": {
			"displayName": display_name,
		},
	})

func send_lobby_join(lobby_id: String, display_name: String):
	socket.send("lobby-join", {
		"lobbyId": lobby_id,
		"client": {
			"displayName": display_name,
		},
	})

func send_lobby_leave():
	socket.send("lobby-leave", null)
